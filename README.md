## CSE 112 Group 1 ##

[Meet our team!](admin/team.md)

[Try our app!](https://cse110-sp23-group02.github.io/cse110-sp23-group02/source/index.html)

[Checkout our Docs!](https://cse110-sp23-group02.github.io/cse110-sp23-group02/docs/jsdoc/index.html)


## In Progress Feature List ##

- Daily fortune on first log in

- Login/Sign up page

- Account tracking information about daily fortune

- Responsive design for screen compatibility across all devices (mobile, tablet, web, etc.)
